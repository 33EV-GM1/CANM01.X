/*! \file  CANM01.h
 *
 *  \brief Application header for CANM01
 *
 *
 *  \author jjmcd
 *  \date January 29, 2016, 11:48 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CANM01_H
#define	CANM01_H

#ifdef	__cplusplus
extern "C"
{
#endif
  
#define PROGRESS_DELAY 500  /* Delay time to show progress messages */
#define NUM_OF_ECAN_BUFFERS 32  /* Number of message buffers available */

void initialization(void);
void initializeClock(void);
void remapCAN(void);
void initializeCAN(void);
void sendCAN(void);


#ifdef	__cplusplus
}
#endif

#endif	/* CANM01_H */

