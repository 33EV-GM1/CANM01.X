/*! \file  sendCAN.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 29, 2016, 3:02 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "CANM01.h"

extern unsigned int ecan1msgBuf[NUM_OF_ECAN_BUFFERS][8];
/*! sendCAN - */

/*!
 *
 */
void sendCAN(void)
{

  /* Write to message buffer 0 */
  /* CiTRBnSID = 0bxxx1 0010 0011 1100
  IDE = 0b0
  SRR = 0b0
  SID<10:0>= 0b100 1000 1111 */
  ecan1msgBuf[0][0] = 0x123C;
  /* CiTRBnEID = 0bxxxx 0000 0000 0000
  EID<17:6> = 0b0000 0000 0000 */
  ecan1msgBuf[0][1] = 0x0000;
  /* CiTRBnDLC = 0b0000 0000 xxx0 1111
  EID<17:6> = 0b000000
  RTR = 0b0
  RB1 = 0b0
  RB0 = 0b0
  DLC = 0b1111 */
  ecan1msgBuf[0][2] = 0x0008;
  /* Write message data bytes */

  ecan1msgBuf[0][3] = 0xabcd;
  ecan1msgBuf[0][4] = 0xabcd;
  ecan1msgBuf[0][5] = 0xabcd;
  ecan1msgBuf[0][6] = 0xabcd;

  /* Request message buffer 0 transmission */
  C1TR01CONbits.TXREQ0 = 0x1;
}
