/*! \file  initialization.c
 *
 *  \brief Initialization for CANM01
 *
 *
 *  \author jjmcd
 *  \date January 29, 2016, 11:51 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/LCD.h"
#include "CANM01.h"

/*! initialization - */

/*!
 *
 */
void initialization(void)
{
  initializeClock();                /* Set clock to 70 MIPS                 */
  
  LCDinit();                        /* Make the LCD available               */
  LCDclear();                       /* Inform user of progress              */
  LCDputs("LCD initialized ");
  Delay_ms(PROGRESS_DELAY);
  
  remapCAN();
  LCDhome();
  LCDputs("CAN remapped    ");
  Delay_ms(PROGRESS_DELAY);

  initializeCAN();
  LCDhome();
  LCDputs("CAN initialized    ");
  Delay_ms(PROGRESS_DELAY);
}
