/*! \file  initializeCAN.c
 *
 *  \brief Initialize the CAN peripheral
 *
 *
 *  \author jjmcd
 *  \date January 29, 2016, 1:45 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "CANM01.h"

/* This is the ECAN message buffer declaration. Note the buffer alignment. */
unsigned int ecan1msgBuf[NUM_OF_ECAN_BUFFERS][8]
__attribute__((aligned(NUM_OF_ECAN_BUFFERS * 16)));


/*! initializeCAN - */

/*!
 *
 */
void initializeCAN(void)
{
  /* Set up the ECAN1 module to operate at 250 kbps. The ECAN module
   *  should be first placed in configuration mode. */
  C1CTRL1bits.REQOP = 4;
  while (C1CTRL1bits.OPMODE != 4);
  C1CTRL1bits.WIN = 0;
  /* Set up the CAN module for 250kbps speed with 10 Tq per bit. */
  C1CFG1 = 0x47;
  //C1CFG1bits.BRP = 7;   /* TQ = 2 x 8 x 1/Fcan   */
  //C1CFG1bits.SJW = 1;   /* Length is 1 x TQ */
  // BRP = 8 SJW = 2 Tq
  C1CFG2 = 0x2D2;
  // C1CFG2bits.PRSEG = 2; /* Propagation time segment length is 3*TQ */
  // C1CFG2bits.SEG1PH = 2; /* Phase segment 1 length is 3*TQ */
  // C1CFG2bits.SAM = 1; /* Bus line is sampled 3 times at the sample point */
  // C1CFG2bits.SEG2PHTS = 1; /*Phase segment 2 time select is freely programmable */
  // C1CFG2bits.SEG2PH = 2; /* Phase segment 2 length is 3*TQ */
  // C1CFG2bits.WAKFIL = 0; /* CAN bus line filter not used for wakeup */
  C1FCTRL = 0xC01F;
  //C1FCTRLbits.FSA = 0x1f; /* FIFO start red buffer RB31 */
  //C1FCTRLbits.DMABS = 0x06;  /* 32 buffers in device RAM */

  // No FIFO, 32 Buffers
  /* Assign 32x8word Message Buffers for ECAN1 in device RAM. This example
   *  uses DMA0 for TX.  Refer to 21.8.1 ?DMA Operation for Transmitting
   *  Data? for details on DMA channel configuration for ECAN transmit. */
  DMA0CONbits.SIZE = 0x0;
  DMA0CONbits.DIR = 0x1;
  DMA0CONbits.AMODE = 0x2;
  DMA0CONbits.MODE = 0x0;
  DMA0REQ = 70;
  DMA0CNT = 7;
  DMA0PAD = (volatile unsigned int) &C1TXD;
  DMA0STAL = (unsigned int) &ecan1msgBuf;
  DMA0STAH = (unsigned int) &ecan1msgBuf;
  DMA0CONbits.CHEN = 0x1;

  /* Configure Message Buffer 0 for Transmission and assign priority */
  C1TR01CONbits.TXEN0 = 0x1; /* Buffer 0 is transmit buffer */
  C1TR01CONbits.TX0PRI = 0x3; /* Highest message priority */
  /* At this point the ECAN1 module is ready to transmit a message. Place
   *  the ECAN module in Normal mode. */
  C1CTRL1bits.REQOP = 0;
  while (C1CTRL1bits.OPMODE != 0);
}
