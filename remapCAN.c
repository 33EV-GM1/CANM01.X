/*! \file  remapCAN.c
 *
 *  \brief Map CAN I/O pins
 * 
 * CAN transmit will appear on Arduino pin A6
 * CAN receive  will appear on Arduino pin A1
 *
 *  \author jjmcd
 *  \date January 29, 2016, 12:09 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! remapCAN - Map CAN Tx and Rx pins to RB4 and RA1*/
/*! Map CAN TX to RB4 and CAN RX to RA1
 * 
 * The CAN pins have no permanent assignments so they must be
 * mapped to physical pins before use.  Generally, the A0-A7
 * Arduino pins tend to be unused, but A0-A5 can be mapped for
 * input only, so A6 was chosen for Tx.
 *                                    PPS
 * Function Register Field     Value  Name  Arduino #
 *   C1RX   RPINR26 C1RXR<7:0> 010001 RPI17    A1
 *   C1TX   RPOR1   RP36R<5:0> 001110 RP36     A6
 */
void remapCAN(void)
{
  /* Map C1TX to RB4 */
  RPOR1bits.RP36R = 0x0e;
  /* Map C2RX to RA1 */
  RPINR26bits.C1RXR = 0x11;
}
