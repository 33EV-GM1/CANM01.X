/*! \file  configBits.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 9:55 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CONFIGBITS_H
#define	CONFIGBITS_H

#ifdef	__cplusplus
extern "C"
{
#endif


// Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
// Watchdog timer off
#pragma config FWDTEN = OFF
// Deadman timer off
#pragma config DMTEN = DISABLE



#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGBITS_H */

