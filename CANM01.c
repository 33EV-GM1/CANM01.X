/*! \file  CANM01.c
 *
 *  \brief CAN central controller test 01
 *
 *
 *  \author jjmcd
 *  \date January 29, 2016, 11:47 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "configBits.h"
#include "CANM01.h"
#include "../include/LCD.h"

/*! main - */

/*!
 *
 */
int main(void)
{
  initialization();
  
  while(1)
    {
      sendCAN();

      LCDhome();
      LCDputs("Message sending    ");

      /* The following shows an example of how the TXREQ bit can be polled to
       *  check if transmission is complete. */
      while (C1TR01CONbits.TXREQ0 == 1)
        ;
      LCDhome();
      LCDputs("Message sent    ");
      Delay_ms(PROGRESS_DELAY);
      
      LCDclear();
      Delay_ms(2000);
    }

  return 0;
}
