/*! \file  initializeClock.c
 *
 *  \brief Set the clock for the CANM01 application
 * 
 * Shamelessly stolen from EEPROMsample.X
 *
 *  \author jjmcd
 *  \date January 11, 2016, 11:24 AM
 *
 */
#include <xc.h>

/*! EEPROMsampleInitializeClock - Set the clock PLL chain for 70 MIPS */
/*! The configuration bits set the dsPIC to use the "Fast RC" oscillator
 *  with PLL.  This function adjusts the PLL chain to provide 70 MIPS
 *  processor speed.  The chain has a multiplier and a number of
 *  dividers allowing for a processor clock from 15 to 140 MHz,
 *  resulting in an instruction rate of 7.5 to 70 MIPS. (The output
 *  of the PLL must be between 120 and 340 MHz).  For slower speeds,
 *  the FRC can be used without the PLL allowing instruction rates
 *  from 29 kIPS to 3.7 MIPS.  Even slower speeds can be achieved with
 *  an external clock.
 * 
 *        ---  PLL Divider / Multiplier chain  ---
 * 
 * FRC  ->  FRCDIV  ->  PLLPRE  ->  PLLFBD  ->  PLLPOST  ->  processor
 * 
 *           FRC          PLL        PLL          PLL
 * 7.37    divider     Prescaler  Multiplier   Postscaler
 * MHz
 *          1-128        2-33        2-512        2-8
 */
void initializeClock(void)
{
  /* The FRC oscillator provides a 7.37 MHz clock.  The PLL
   * chain gives a number of divide and multiply options
   * to allow selecting an appropriate clock.              */
  
  /* The Fast RC post scaler can divide the 7.37 MHz clock
   * by a umber between 1 and 128.  FRCdiv is set to 0
   * to divide by 1, 1 to divide by 2, 2 to divide 
   * by 4, etc.                                             */
  CLKDIVbits.FRCDIV = 0;    /* Divide by 1 = 7.37MHz        */
  
  /* The PLL prescaler can divide by a number from 2 to
   * 33.  Setting 0 divides by 2, 1 by 3, etc.              */
  CLKDIVbits.PLLPRE = 0;    /* Divide by 2 = 3.685 MHz      */
  
  /* The PLL feedback divider causes the frequency used to
   * be multiplied, by dividing the feedback.  Choices are
   * 2 (register value of 0) to 512 (register value of
   * 511).  The result must end up with a final frequency
   * within the processor specifications (<=140).           */
  PLLFBD = 74;              /* Multiply by 76 = 280         */
  
  /* The PLL postscaler can divide the result by a value
   * between 2 and 8, with a register value of 0 dividing
   * by 2, 1 by 4, and 3 by 8.  A register value of 2 is
   * not permitted.                                         */
  CLKDIVbits.PLLPOST = 0;   /* Divide by 2 = 140            */
  
  /* Each instruction takes 2 clock cycles, so the 140 MHz
   * clock results in an instruction rate of 70 MIPS        */
}
